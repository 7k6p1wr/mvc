package Controller;

import Model.Model;
import View.View;

import java.util.Scanner;

public class Controller {

    private Model model;
    private View view;
    private Scanner sc;
    private boolean isEnd = true;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
        sc = new Scanner(System.in);
    }

    public void startGame(){
        view.printInfo(View.HELLO);
        view.printInfo(View.INPUT);
        int input;
        while (isEnd){
            try{
                input = Integer.parseInt(sc.next());
                if(input == model.getValue()){
                    isEnd = false;
                    view.printInfo(View.WIN, model.getTrust());
                }else{
                    model.incrementTrust();
                    if(input > model.getValue()){
                        view.printInfo(View.LESS, input);
                    }else{
                        view.printInfo(View.MORE, input);
                    }
                }
            }catch (Exception e){
                view.printInfo(View.TRY);
            }
        }
    }
}
