package Model;

import java.util.Random;

public class Model {

    private int value;
    private int trust;
    private Random random;

    public Model() {
        trust = 0;
        random = new Random();
        value = random.nextInt(100);
    }

    public int getValue() {
        return value;
    }

    public int getTrust() {
        return trust;
    }
    public void incrementTrust() {
        setTrust(getTrust()+1);
    }
    private void setTrust(int trust) {
        this.trust = trust;
    }
}
