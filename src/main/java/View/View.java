package View;

public class View {

    public static String HELLO = "Welcome to the game \"Guess the number from 0 to 100\".\n" +
            "You have to guess the hidden number by entering numbers.\n" +
            "Good luck!";
    public static String INPUT = "Please enter a number from 0 to 100";
    public static String WIN = "You won. Number of attempts: ";
    public static String MORE = "Wrong answer. The number is greater than ";
    public static String LESS = "Wrong answer. number less than ";
    public static String TRY = "You didn't enter a number. Try again.";

    public void printInfo(String message){
        System.out.println(message);
    }
    public void printInfo(String message, int count){
        System.out.println(message + count);
    }

}
